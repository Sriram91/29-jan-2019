package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.When;

public class MyLeadsPage extends Annotations{ 

	public MyLeadsPage() {
       PageFactory.initElements(driver, this);
	} 
	
@FindBy(how=How.LINK_TEXT, using="Create Lead")  WebElement eleCreateLeads;
	@When("I Click Create lead tab in the find lead page")
	
	public CreateLeadPage clickCreateLeads() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateLeads);  
		return new CreateLeadPage();



	}
@FindBy(how=How.LINK_TEXT, using="Merge Leads")  WebElement eleMergeLead;
	
	public MergeLeadPage clickMergeLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleMergeLead);  
		return new MergeLeadPage();

}
}







