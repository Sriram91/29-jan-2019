Feature: Creating a lead in Test Application


#Background:
#Given open the browser
#And maximize the browser
#And load the URL

#Scenario: To test the creating lead in the leaptap application
#And Enter the user name 
#And Enter the password
#When I Click Login button
#Then Verify login page is successful display
#When I Click CRMSFA link
#Then Page navigate to the find lead page
#When I Click lead tab in the find lead page
#When I Click Create lead tab in the find lead page
#And I enter the first name sriram
#And I enter the last name  gokul
#And I enter the company name Indium
#When I click Create lead
#Then Verify create lead page is created successfully

		
Scenario Outline: To test the creating lead in the leaptap application

And Enter the user name <uname>
And Enter the password <pwd>
When I Click Login button
#Then Verify login page is successful display
When I Click CRMSFA link
#Then Page navigate to the find lead page
When I Click lead tab in the find lead page
When I Click Create lead tab in the find lead page
And I enter the first name <fname>
And I enter the last name <lname>
And I enter the company name <cname>
When I click Create lead
#Then Verify create lead page is created successfully
Examples:
		|uname|pwd|fname|lname|cname|
		|DemoSalesManager|crmsfa|kannan|gokul|hcl|	

		
